FROM debian:bullseye
LABEL maintainer="Michael Fisher <mfisher@lvtk.org>"

RUN apt-get update && \
    apt-get -y -q upgrade && \
    \
    apt-get install -y -q --no-install-recommends locales meson \
    pkg-config libboost-dev build-essential clang gcc g++ \
    python3 python2 python3-sphinx pip doxygen graphviz git \
    python3-sphinx-rtd-theme python3-lxml python3-rdflib python3-markdown \
    libpugl-dev lv2-dev liblilv-dev libsuil-dev curl \
    liblua5.4-dev lua5.4 lua-ldoc lua-markdown \
    libx11-xcb-dev

RUN apt-get install -y -q --no-install-recommends mingw-w64
RUN update-alternatives --set x86_64-w64-mingw32-gcc /usr/bin/x86_64-w64-mingw32-gcc-posix; \
    update-alternatives --set x86_64-w64-mingw32-g++ /usr/bin/x86_64-w64-mingw32-g++-posix; \
    update-alternatives --set i686-w64-mingw32-gcc /usr/bin/i686-w64-mingw32-gcc-posix; \
    update-alternatives --set i686-w64-mingw32-g++ /usr/bin/i686-w64-mingw32-g++-posix

RUN apt-get autoremove -y -q --purge && \
    apt-get -y -q clean && \
    rm -rf /var/lib/apt/lists/*

RUN curl -L https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage > /usr/bin/linuxdeploy-x86_64.AppImage && \
    chmod +x /usr/bin/linuxdeploy-x86_64.AppImage && \
    ln -s /usr/bin/linuxdeploy-x86_64.AppImage /usr/bin/linuxdeploy

RUN pip install sphinx_lv2_theme

WORKDIR /workdir
RUN mkdir -p /stage /workdir
VOLUME [ "/stage", "/workdir" ]
CMD ["/bin/bash"]
